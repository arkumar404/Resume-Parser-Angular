import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { CanditatesDetailService } from '../canditates-detail.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';


@Component({
  selector: 'app-resume-checker',
  templateUrl: './resume-checker.component.html',
  styleUrls: ['./resume-checker.component.css']
})

export class ResumeCheckerComponent implements OnInit {

  candidatesData:any = [];

  displayedColumns: string[] = ['name', 'Contact', 'email', 'skills'];
  dataSource!:MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator! : MatPaginator;
  @ViewChild(MatSort) matSort! : MatSort;

  constructor(private candidateService: CanditatesDetailService) { }

  ngOnInit(): void {
  
    this.candidatesData = this.candidateService.getCandidate().subscribe(
    (data) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.matSort;
      this.dataSource.paginator = this.paginator;
      }
    )
  }

  applyFilter(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getDropdownValue(event: Event){
    const dropDownValue = (event.target as HTMLInputElement).value;
    console.log("======================================");
    console.log(dropDownValue);
  }
}
