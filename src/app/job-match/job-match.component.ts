import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { CanditatesDetailService } from '../canditates-detail.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { RankingService } from '../ranking.service';

@Component({
  selector: 'app-job-match',
  templateUrl: './job-match.component.html',
  styleUrls: ['./job-match.component.css']
})

export class JobMatchComponent implements OnInit {

  candidatesData:any = [];
  
  displayedColumns: string[] = ['name', 'Contact', 'email', 'skills'];
  dataSource!:MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator! : MatPaginator;
  @ViewChild(MatSort) matSort! : MatSort;

  constructor(private candidateService: CanditatesDetailService, private router: Router, private rankingService: RankingService) { }

  ngOnInit(): void {
    this.candidatesData = this.candidateService.getCandidate().subscribe(
      (data) => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.sort = this.matSort;
        this.dataSource.paginator = this.paginator;
        }
      )
  }

  applyFilter(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  backToList(){
    console.log("backToList method clicked");
    this.router.navigate(['/jobs'])
  }

}
